FROM node:18-alpine
EXPOSE 3000
WORKDIR /app
RUN apk update && apk upgrade && apk add bash vim --no-cache
COPY ./src/ .
RUN npm install -g http-proxy-middleware serve
RUN npm install
RUN npm run build
#ENTRYPOINT ["npm", "run", "start"]
ENTRYPOINT ["npx","serve", "-s", "build"]
