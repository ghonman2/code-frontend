const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    createProxyMiddleware('/board', {
      target: 'http://13.59.201.111/backend',
      changeOrigin: true,
    }),
  );
};
